﻿using AutoMapper;
using TestAPI.Models.Entity;
using TestAPI.Models.ViewModels;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace TestAPI
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<Record, RecordVM>().ReverseMap();
		}
	}
}
