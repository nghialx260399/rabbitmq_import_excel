﻿using RabbitMQ.Client;
using System.Text;

namespace TestAPI.Services.Rabbitmq
{
    public class RabbitMQPublisherService : IDisposable
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _queueName;

        public RabbitMQPublisherService(IConfiguration configuration, string queueName)
        {
            var factory = new ConnectionFactory()
            {
                HostName = "localhost",
                //Port = 15672,
                //UserName = "guest",
                //Password = "guest"
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _queueName = queueName;

            _channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
        }

        public void PublishMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(exchange: "sample-exchange", routingKey: _queueName, basicProperties: null, body: body);
        }

        public void Dispose()
        {
            _channel.Close();
            _connection.Close();
        }
    }
}
