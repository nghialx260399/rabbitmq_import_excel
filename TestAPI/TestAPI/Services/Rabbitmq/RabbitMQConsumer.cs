﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using Telegram.Bot.Types;
using Newtonsoft.Json;
using TestAPI.Models.Entity;
using TestAPI.Services.Excels;

namespace TestAPI.Services.Rabbitmq
{
    public class RabbitMQConsumer
    {
        private readonly IModel _channel;
        private readonly string _queueName;

        public RabbitMQConsumer(string hostName, string queueName)
        {
            var factory = new ConnectionFactory() { HostName = hostName };
            var connection = factory.CreateConnection();
            _channel = connection.CreateModel();
            _queueName = queueName;
        }

        public void StartConsuming(Action<string> messageHandler)
		{
			var consumer = new EventingBasicConsumer(_channel);
			consumer.Received += (model, ea) =>
			{
				var body = ea.Body.ToArray();
				var message = Encoding.UTF8.GetString(body);
				messageHandler(message);
			};

            _channel.BasicConsume(queue: _queueName, autoAck: true, consumer: consumer);

		}

        public void StopConsuming()
        {
            _channel.Close();
            _channel.Dispose();
        }

        public void Dispose()
        {
            _channel.Close();
            _channel.Dispose();
        }
    }
}
