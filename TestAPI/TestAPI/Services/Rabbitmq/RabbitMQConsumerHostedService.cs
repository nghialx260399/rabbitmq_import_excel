﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading.Channels;
using Telegram.Bot.Types;
using TestAPI.Models;
using TestAPI.Models.Entity;
using TestAPI.Services.Excels;

namespace TestAPI.Services.Rabbitmq
{
    public class RabbitMQConsumerHostedService : IHostedService, IDisposable
    {
		private IServiceScopeFactory services { get; }
		private IModel _channel;

		public RabbitMQConsumerHostedService(IServiceScopeFactory services)
        {
            this.services = services;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
			var connectionFactory = new ConnectionFactory
			{
				HostName = "localhost",
				// Add other RabbitMQ configuration as needed
			};

			var connection = connectionFactory.CreateConnection();
			_channel = connection.CreateModel();

			// Declare the exchange
			_channel.ExchangeDeclare(exchange: "sample-exchange", type: ExchangeType.Fanout);

			// Declare a queue
			var queueName = _channel.QueueDeclare().QueueName;

			// Bind the queue to the exchange
			_channel.QueueBind(queue: queueName, exchange: "sample-exchange", routingKey: "");

			// Set up a consumer
			var consumer = new EventingBasicConsumer(_channel);
			consumer.Received += async (model, ea) =>
			{
				var body = ea.Body.ToArray();
				var message = Encoding.UTF8.GetString(body);

				await ProcessMessageAsync(message);

				_channel.BasicAck(ea.DeliveryTag, multiple: false);
			};

			// Start consuming messages
			_channel.BasicConsume(queue: queueName, autoAck: false, consumer: consumer);

			return Task.CompletedTask;
		}

		private async Task ProcessMessageAsync(string message)
		{
			try
			{
				using (var scope = services.CreateScope())
				{
					var excelService = scope.ServiceProvider.GetRequiredService<IExcelService>();

					// Deserialize the message
					var data = JsonConvert.DeserializeObject<IList<Record>>(message);

					// Perform data insertion logic
					await excelService.CreateRange(data);
				}
			}
			catch (Exception ex)
			{
			}
		}

		public Task StopAsync(CancellationToken cancellationToken)
        {
			_channel?.Close();
			_channel?.Dispose();
			return Task.CompletedTask;
        }

        public void Dispose()
        {
			_channel?.Close();
			_channel?.Dispose();
		}

    }

}
