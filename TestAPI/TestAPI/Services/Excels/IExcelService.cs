﻿using OfficeOpenXml;
using TestAPI.Models.Entity;

namespace TestAPI.Services.Excels
{
	public interface IExcelService
	{
		Task<IList<Record>> GetAll();

		Task<long> CountData();

		Task<Record> GetById(int id);

		Task<bool> CreateRange(IList<Record> product);

		Task ImportExcel(IFormFile formFile, CancellationToken cancellationToken);

		Task ImportExcelAsync(IFormFile formFile, CancellationToken cancellationToken);

		Task ReadAndProcessDataAsync(ExcelWorksheet worksheet, int startRow, int endRow);
	}
}