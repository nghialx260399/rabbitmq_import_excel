﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OfficeOpenXml;
using System.Threading;
using Telegram.Bot.Types;
using TestAPI.Models;
using TestAPI.Models.Entity;
using TestAPI.Models.ViewModels;
using TestAPI.Services.Rabbitmq;

namespace TestAPI.Services.Excels
{
	public class ExcelService : IExcelService
	{
		private readonly ApplicationDbContext _context;
		private readonly RabbitMQPublisherService _rabbitMQPublisher;
		private IServiceScopeFactory services { get; }
		public ExcelService(ApplicationDbContext context, RabbitMQPublisherService rabbitMQPublisher, IServiceScopeFactory services)
		{
			_context = context ?? throw new ArgumentNullException();
			_rabbitMQPublisher = rabbitMQPublisher;
			this.services = services;
		}

		public async Task<long> CountData()
		{
			return await _context.Records.CountAsync();
		}

		public async Task ImportExcelAsync(IFormFile formFile, CancellationToken cancellationToken)
		{

			using (var stream = new MemoryStream())
			{
				await formFile.CopyToAsync(stream, cancellationToken);

				using (var package = new ExcelPackage(stream))
				{
					var worksheet = package.Workbook.Worksheets[0]; // Specify the worksheet you want to process
					int rowCount = worksheet.Dimension.Rows;

					int batchSize = rowCount/4; // Adjust this as needed
					Task.WhenAll(ReadAndProcessDataAsync(worksheet, 1, batchSize), ReadAndProcessDataAsync(worksheet, batchSize + 1, 2*batchSize),
						ReadAndProcessDataAsync(worksheet, 2*batchSize + 1, 3*batchSize), ReadAndProcessDataAsync(worksheet, 3*batchSize + 1, rowCount));

					//for (int startRow = 1; startRow <= rowCount; startRow += batchSize)
					//{ 
					//	Console.WriteLine("--------------------------" + startRow + "------------------------------------------");
     //                   int endRow = Math.Min(startRow + batchSize - 1, rowCount);

					//	// Read and process rows from startRow to endRow asynchronously
					//	var data = await ReadAndProcessDataAsync(worksheet, startRow, endRow);

					//	// Insert data into the database asynchronously
					//	await CreateRange(data);
					//}
				}
			}
		}

		public async Task<bool> CreateRange(IList<Record> product)
		{
			try
			{
				using (var scope = services.CreateScope())
				{
					var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

					await dbContext.Records.AddRangeAsync(product);
					return await dbContext.SaveChangesAsync() > 0;
				}
				
			}
			catch (Exception)
			{
				return false;
			}
		}

		public async Task ReadAndProcessDataAsync(ExcelWorksheet worksheet, int startRow, int endRow)
		{
			var data = new List<Record>();
			for (int row = startRow; row <= endRow; row++)
			{
				// Create MyDataModel objects and add them to the list
				var model = new Record {
					FName = worksheet.Cells[row, 1].Value.ToString().Trim(),
					LName = worksheet.Cells[row, 2].Value.ToString().Trim(),
					Address = worksheet.Cells[row, 3].Value.ToString().Trim(),
					Process = int.Parse(worksheet.Cells[row, 4].Value.ToString().Trim())
				};
				
				//for(int i = 1; i < 1000000; i++)
				//{
				//	var a = 5;
				//}

				CreateSingle(model);

				//data.Add(model);
			}

			//CreateRange(data);
		}

		public async Task<bool> CreateSingle(Record product)
		{
			try
			{
				using (var scope = services.CreateScope())
				{
					var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

					await dbContext.Records.AddAsync(product);
					return await dbContext.SaveChangesAsync() > 0;
				}

			}
			catch (Exception)
			{
				return false;
			}
		}

		public async Task<IList<Record>> GetAll()
		{
			return await _context.Records.ToListAsync();
		}

		public async Task<Record> GetById(int id)
		{
			return await _context.Records.FindAsync(id);
		}

		public async Task ImportExcel(IFormFile formFile, CancellationToken cancellationToken)
		{
			var list = new List<RecordVM>();
            Console.Out.WriteLineAsync();
            using (var stream = new MemoryStream())
			{
				await formFile.CopyToAsync(stream, cancellationToken);

				using (var package = new ExcelPackage(stream))
				{
					ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
					var rowCount = worksheet.Dimension.Rows;

					for (int row = 2; row <= rowCount; row++)
					{
						list.Add(new RecordVM
						{
							FName = worksheet.Cells[row, 1].Value.ToString().Trim(),
							LName = worksheet.Cells[row, 2].Value.ToString().Trim(),
							Address = worksheet.Cells[row, 3].Value.ToString().Trim()
						});
					}
				}
			}

			_rabbitMQPublisher.PublishMessage(JsonConvert.SerializeObject(list));
		}
	}
}
