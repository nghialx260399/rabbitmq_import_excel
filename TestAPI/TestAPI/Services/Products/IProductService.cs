﻿using TestAPI.Models.Entity;

namespace TestAPI.Services.Products
{
    public interface IProductService
    {
        Task<IList<Product>> GetAll();

        Task<Product> GetById(int id);

        Task<bool> Create(Product product);

        Task<bool> Update(Product product);

        Task<bool> Delete(int id);
    }
}