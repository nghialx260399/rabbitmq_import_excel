﻿using Microsoft.EntityFrameworkCore;
using TestAPI.Models;
using TestAPI.Models.Entity;

namespace TestAPI.Services.Products
{
    public class ProductService : IProductService
    {
        private readonly ApplicationDbContext _context;
        public ProductService(ApplicationDbContext _context)
        {
            _context = _context ?? throw new ArgumentNullException();
        }
        public async Task<bool> Create(Product product)
        {
            try
            {
                await _context.Products.AddAsync(product);
                return _context.SaveChanges() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                Product product = await _context.Products.FindAsync(id);
                if (product != null)
                {
                    _context.Products.Remove(product);
                    return _context.SaveChanges() > 0;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<Product>> GetAll()
        {
            return await _context.Products.ToListAsync();
        }

        public async Task<Product> GetById(int id)
        {
            return await _context.Products.FindAsync(id);
        }

        public async Task<bool> Update(Product product)
        {
            try
            {
                _context.Products.Update(product);
                return _context.SaveChanges() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
