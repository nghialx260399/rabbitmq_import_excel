﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestAPI.Filters;
using TestAPI.Models.Entity;
using TestAPI.Models.ViewModels;
using TestAPI.Services.Excels;
using TestAPI.Services.Rabbitmq;

namespace TestAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IExcelService excelService;
        public HomeController(RabbitMQPublisherService rabbitMQPublisher, IExcelService excelService)
        {
			this.excelService = excelService;
        }

        [HttpGet]
        [ServiceFilter(typeof(FilterExample))]
        public async Task<IActionResult> ExportToExcel()
        {
            List<RecordVM> recordobj = new List<RecordVM>();
            recordobj.Add(new RecordVM { FName = "Smith", LName = "Singh", Address = "Knpur" });
            recordobj.Add(new RecordVM { FName = "John", LName = "Kumar", Address = "Lucknow" });
            recordobj.Add(new RecordVM { FName = "Vikram", LName = "Kapoor", Address = "Delhi" });
            recordobj.Add(new RecordVM { FName = "Tanya", LName = "Shrma", Address = "Banaras" });
            recordobj.Add(new RecordVM { FName = "Malini", LName = "Ahuja", Address = "Gujrat" });
            recordobj.Add(new RecordVM { FName = "Varun", LName = "Katiyar", Address = "Rajasthan" });
            recordobj.Add(new RecordVM { FName = "Arun  ", LName = "Singh", Address = "Jaipur" });
            recordobj.Add(new RecordVM { FName = "Ram", LName = "Kapoor", Address = "Panjab" });
            recordobj.Add(new RecordVM { FName = "Vishakha", LName = "Singh", Address = "Banglor" });
            recordobj.Add(new RecordVM { FName = "Tarun", LName = "Singh", Address = "Kannauj" });
            recordobj.Add(new RecordVM { FName = "Mayank", LName = "Dubey", Address = "Farrukhabad" });
			var stream = new MemoryStream();

			using (var package = new ExcelPackage(stream))
			{
				var workSheet = package.Workbook.Worksheets.Add("Sheet1");
				workSheet.Cells.LoadFromCollection(recordobj, true);
				package.Save();
			}
			stream.Position = 0;
			string excelName = $"UserList-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";

			return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
		}

		[HttpGet]
		public async Task<IActionResult> GetAll()
		{
			return Ok(await excelService.GetAll());
		}

		[HttpGet]
		public async Task<IActionResult> CountData()
		{
			return Ok(await excelService.CountData());
		}

		[HttpPost]
		public async Task<IActionResult> ImportExcel(IFormFile formFile, CancellationToken cancellationToken)
        {
			if (formFile == null || formFile.Length <= 0)
			{
				return BadRequest("formfile is empty");
			}

			if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
			{
				return BadRequest("Not Support file extension");
			}

			excelService.ImportExcelAsync(formFile, cancellationToken);
			return Ok();
        }
    }
}
