﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot;

namespace TestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendMessageTelegramController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> SendMessage(long chatId, string message)
        {
            try
            {
                var botToken = "6757279513:AAELo-m6py0QVFEpLnWsAyvgsOLLV18ktps";
                var botClient = new TelegramBotClient(botToken);

                await botClient.SendTextMessageAsync(chatId, message);

                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
           
        }
    }
}
