﻿namespace TestAPI.Models.Entity
{
    public class Product
    {
        public int Id { get; set; }

        public int Name { get; set; }

        public int Price { get; set; }
    }
}
