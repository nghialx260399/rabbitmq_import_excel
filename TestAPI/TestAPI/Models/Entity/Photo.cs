﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TestAPI.Models.Entity
{
    public class Photo
    {
        public int Id { get; set; }

        public byte[] Bytes { get; set; }

        public int ProductId { get; set; }
        
        public Product? Product { get; set; }
    }
}
