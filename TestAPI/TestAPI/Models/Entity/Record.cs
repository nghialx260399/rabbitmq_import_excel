﻿namespace TestAPI.Models.Entity
{
    public class Record
    {
        public int Id { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Address { get; set; }
        public string? BatchId { get; set; }
        public int? Process { get; set; }
    }
}
