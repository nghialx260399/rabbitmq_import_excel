﻿using Microsoft.AspNetCore.Identity;

namespace TestAPI.Models.Entity
{
    public class ApplicationUser:IdentityUser
    {
        public string? RefreshToken { get; set; }

        public DateTime RefreshTokenExpiryTime { get; set; }
    }
}
