﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Reflection.Emit;
using TestAPI.Models.Entity;

namespace TestAPI.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Record> Records { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.Entity<Photo>()
            //.HasOne<Product>(p => p.Product)
            //.WithMany(p => p.Photos)
            //.HasForeignKey(p => p.ProductId)
            //.OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(builder);
        }
    }
}
