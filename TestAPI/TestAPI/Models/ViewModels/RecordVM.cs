﻿namespace TestAPI.Models.ViewModels
{
	public class RecordVM
	{
		public string FName { get; set; }
		public string LName { get; set; }
		public string Address { get; set; }
	}
}
