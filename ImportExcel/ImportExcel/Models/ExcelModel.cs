﻿namespace ImportExcel.Models
{
    public class ExcelModel
    {
        public int? Id { get; set; }

        public string? PhuongXa { get; set; }

        public string? QuanHuyen { get; set; }

        public string? TinhThanh { get; set; }

        public string? LoaiHinhCoSo { get; set; }

        public string? MaCoSo { get; set; }

        public string? TenCoSo { get; set; }

        public string? TenChuCoSo { get; set; }

        public string? SDT { get; set; }

        public string? DiaChi { get; set; }

        public DateTime? NgayHieuLuc { get; set; }

        public string? SoLuongKhach { get; set; }

        public string? SoLuongThongBao { get; set; }
    }
}
