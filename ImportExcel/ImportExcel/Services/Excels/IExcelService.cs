﻿using ImportExcel.Models;
using OfficeOpenXml;

namespace ImportExcel.Services.Excels
{
    public interface IExcelService
    {
        Task<IList<ExcelModel>> ImportExcel(IFormFile formFile, CancellationToken cancellationToken, string type);

        Task CustomExcelExport(ExcelWorksheet sheet);
    }
}