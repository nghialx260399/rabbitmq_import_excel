﻿using ImportExcel.Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace ImportExcel.Services.Excels
{
    public class ExcelService: IExcelService
    {
		public async Task CustomExcelExport(ExcelWorksheet sheet)
		{
			using (var range = sheet.Cells[1, 1, 1, 13])  //Address "A1:A5"
			{
				range.Style.Font.Bold = true;
				range.Style.Fill.PatternType = ExcelFillStyle.Solid;
				range.Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
				range.Style.Font.Color.SetColor(Color.White);
			}

			sheet.Cells["A1"].Value = "STT";
			sheet.Cells["B1"].Value = "Phường/ Xã";
			sheet.Cells["C1"].Value = "Quận/ Huyện";
			sheet.Cells["D1"].Value = "Tỉnh Thành";
			sheet.Cells["E1"].Value = "Loại hình cơ sở lưu trú";
			sheet.Cells["F1"].Value = "Mã cơ sở lưu trú";
			sheet.Cells["G1"].Value = "Tên cơ sở lưu trú";
			sheet.Cells["H1"].Value = "Tên chủ cơ sở lưu trú";
			sheet.Cells["I1"].Value = "Số điện thoại cơ sở lưu trú";
			sheet.Cells["J1"].Value = "Địa chỉ chi tiết";
			sheet.Cells["K1"].Value = "Ngày hiệu lực đến";
			sheet.Cells["L1"].Value = "Số lượng khách checkin";
			sheet.Cells["M1"].Value = "Số lượng thông báo";

			sheet.Column(1).Width = 5;
			sheet.Column(2).Width = 20;
			sheet.Column(3).Width = 20;
			sheet.Column(4).Width = 20;
			sheet.Column(5).Width = 20;
			sheet.Column(6).Width = 20;
			sheet.Column(7).Width = 20;
			sheet.Column(8).Width = 20;
			sheet.Column(9).Width = 20;
			sheet.Column(10).Width = 20;
			sheet.Column(11).Width = 20;
			sheet.Column(12).Width = 20;
			sheet.Column(13).Width = 20;

			sheet.Column(11).Style.Numberformat.Format = "mm/dd/yyyy HH:mm";

			sheet.View.FreezePanes(2, 1);
		}

		public async Task<IList<ExcelModel>> ImportExcel(IFormFile formFile, CancellationToken cancellationToken, string type)
        {
            var list = new List<ExcelModel>();
            var dangerList = new List<ExcelModel>();
            try
            {
                using (var stream = new MemoryStream())
                {
                    await formFile.CopyToAsync(stream, cancellationToken);

                    using (var package = new ExcelPackage(stream))
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                        var rowCount = worksheet.Dimension.Rows;

                        for (int row = 2; row <= rowCount; row++)
                        {
							list.Add(new ExcelModel
							{
								Id = row,
								PhuongXa = Convert.ToString(worksheet.Cells[row, 1].Value).Trim(),
								QuanHuyen = Convert.ToString(worksheet.Cells[row, 2].Value).Trim(),
								TinhThanh = Convert.ToString(worksheet.Cells[row, 3].Value).Trim(),
								LoaiHinhCoSo = Convert.ToString(worksheet.Cells[row, 4].Value).Trim(),
								MaCoSo = Convert.ToString(worksheet.Cells[row, 5].Value).Trim(),
								TenCoSo = Convert.ToString(worksheet.Cells[row, 6].Value).Trim(),
								TenChuCoSo = Convert.ToString(worksheet.Cells[row, 7].Value).Trim(),
								SDT = Convert.ToString(worksheet.Cells[row, 8].Value).Trim(),
								DiaChi = Convert.ToString(worksheet.Cells[row, 9].Value).Trim(),
								NgayHieuLuc = DateTime.TryParse(Convert.ToString(worksheet.Cells[row, 10].Value).Trim(), out DateTime dateTime) ? dateTime : DateTime.MinValue,
								SoLuongKhach = Convert.ToString(worksheet.Cells[row, 11].Value).Trim(),
								SoLuongThongBao = Convert.ToString(worksheet.Cells[row, 12].Value).Trim(),
							});
                        }

						if (type == "PhuLy")
						{
							list = list.Where(x=>x.QuanHuyen.ToLower().Contains("Phủ Lý".ToLower())).ToList();
						}
						else if (type == "DuyTien")
						{
							list = list.Where(x => x.QuanHuyen.ToLower().Contains("Duy Tiên".ToLower())).ToList();
						}
						else if (type == "KimBang")
						{
							list = list.Where(x => x.QuanHuyen.ToLower().Contains("Kim Bảng".ToLower())).ToList();
						}
						else if (type == "ThanhLiem")
						{
							list = list.Where(x => x.QuanHuyen.ToLower().Contains("Thanh Liêm".ToLower())).ToList();
						}
						else if (type == "BinhLuc")
						{
							list = list.Where(x => x.QuanHuyen.ToLower().Contains("Bình Lục".ToLower())).ToList();
						}
						else if (type == "LyNhan")
						{
							list = list.Where(x => x.QuanHuyen.ToLower().Contains("Lý Nhân".ToLower())).ToList();
						}

						dangerList = list.Where(x => x.TenCoSo.ToLower().Contains("benhvien".ToLower())
												|| x.TenCoSo.ToLower().Contains("bệnh viện".ToLower())
												|| x.TenCoSo.ToLower().Contains("benh vien".ToLower())
												|| x.TenCoSo.ToLower().Contains("y tế".ToLower())
												|| x.TenCoSo.ToLower().Contains("y te".ToLower())
												|| x.TenCoSo.ToLower().Contains("công ty".ToLower())
												|| x.TenCoSo.ToLower().Contains("cong ty".ToLower())
												|| x.TenCoSo.ToLower().Contains("congty".ToLower())
												|| x.TenCoSo.ToLower().Contains("bến xe".ToLower())
												|| x.TenCoSo.ToLower().Contains("ben xe".ToLower())
												|| x.TenCoSo.ToLower().Contains("nha xe".ToLower())
												|| x.TenCoSo.ToLower().Contains("nhà xe".ToLower())
												).ToList();

						foreach (var danger in dangerList)
						{
							list.Remove(danger);
						}

						list.AddRange(
							new List<ExcelModel>
							{
								new ExcelModel(), 
								new ExcelModel(),
								new ExcelModel()
							}
						);

						list.AddRange(dangerList);

						return list;
                    }
                }
            }
            catch (Exception)
            {
                return new List<ExcelModel>();
            }
            
        }
    }
}
