﻿using ImportExcel.Models;
using ImportExcel.Services.Excels;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Diagnostics;
using System.Drawing;

namespace ImportExcel.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IExcelService _excelService;

        public HomeController(ILogger<HomeController> logger, IExcelService excelService)
        {
            _logger = logger;
            _excelService = excelService;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile formFile, CancellationToken cancellationToken)
        {
            var phuLys = await _excelService.ImportExcel(formFile, cancellationToken, "PhuLy");
            var duyTiens = await _excelService.ImportExcel(formFile, cancellationToken, "DuyTien");
            var kimBangs = await _excelService.ImportExcel(formFile, cancellationToken, "KimBang");
            var thanhLiems = await _excelService.ImportExcel(formFile, cancellationToken, "ThanhLiem");
            var binhLucs = await _excelService.ImportExcel(formFile, cancellationToken, "BinhLuc");
            var lyNhans = await _excelService.ImportExcel(formFile, cancellationToken, "LyNhan");
            var stream = new MemoryStream();

            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Phủ Lý");
                workSheet.Cells.LoadFromCollection(phuLys, true);
                await _excelService.CustomExcelExport(workSheet);

				workSheet = package.Workbook.Worksheets.Add("Duy Tiên");
				workSheet.Cells.LoadFromCollection(duyTiens, true);
				await _excelService.CustomExcelExport(workSheet);

				workSheet = package.Workbook.Worksheets.Add("Kim Bảng");
				workSheet.Cells.LoadFromCollection(kimBangs, true);
				await _excelService.CustomExcelExport(workSheet);

				workSheet = package.Workbook.Worksheets.Add("Thanh Liêm");
				workSheet.Cells.LoadFromCollection(thanhLiems, true);
				await _excelService.CustomExcelExport(workSheet);

				workSheet = package.Workbook.Worksheets.Add("Bình Lục");
				workSheet.Cells.LoadFromCollection(binhLucs, true);
				await _excelService.CustomExcelExport(workSheet);

				workSheet = package.Workbook.Worksheets.Add("Lý Nhân");
				workSheet.Cells.LoadFromCollection(lyNhans, true);
				await _excelService.CustomExcelExport(workSheet);

				package.Save();
            }
            stream.Position = 0;
            string excelName = $"BaoCao-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}